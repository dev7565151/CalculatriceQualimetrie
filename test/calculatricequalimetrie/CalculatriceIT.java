/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatricequalimetrie;

import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ndimb
 */
public class CalculatriceIT {
    
    public CalculatriceIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class Calculatrice.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        Calculatrice instance1 = Calculatrice.getInstance();
        Calculatrice instance2 = Calculatrice.getInstance();
        assertNotNull(instance1);
        assertNotNull(instance2);
        assertSame(instance1, instance2);
    }

    /**
     * Test of addition method, of class Calculatrice.
     */
    @Test
    public void testAddition() {
        System.out.println("addition");
        int a = 5;
        int b = 5;
        Calculatrice instance = Calculatrice.getInstance();
        int expResult = 10;
        int result = instance.addition(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of soustraction method, of class Calculatrice.
     */
    @Test
    public void testSoustraction() {
        System.out.println("soustraction");
        int a = 6;
        int b = 6;
        Calculatrice instance = Calculatrice.getInstance();
        int expResult = 0;
        int result = instance.soustraction(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of division method, of class Calculatrice.
     */
    @Test
    public void testDivision() {
        System.out.println("division");
        double a = 30.0;
        double b = 2.0;
        Calculatrice instance = Calculatrice.getInstance();
        double expResult = 15.0;
        double result = instance.division(a, b);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of multiplication method, of class Calculatrice.
     */
    @Test
    public void testMultiplication() {
        System.out.println("multiplication");
        int a = 2;
        int b = 2;
        Calculatrice instance = Calculatrice.getInstance();
        int expResult = 4;
        int result = instance.multiplication(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of calculMoyenne method, of class Calculatrice.
     */
    @Test
    public void testCalculMoyenne() {
        System.out.println("calculMoyenne");
        List<Integer> nombres = Arrays.asList(3, 4, 9, 8, 15);
        Calculatrice instance = Calculatrice.getInstance();
        double expResult = 7.8;
        double result = instance.calculMoyenne(nombres);
        assertEquals(expResult, result, 0.0);
    }
    
}
